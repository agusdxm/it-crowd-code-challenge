
import { Injectable } from '@angular/core';
import { AngularIndexedDB } from 'angular2-indexeddb';
import { Subject } from 'rxjs';

@Injectable()
export class IndexedDBProvider {

    constructor() { }

    /**
     * Create data base.
     * 
     * @param dbName 
     * @param version 
     */
    public createIndexedDB(dbName: string, version: number) {

        const db =  new AngularIndexedDB(dbName, version);
        return db
    }

    /**
     * Create the indexes for Object Store 
     * @param db 
     * @param version 
     */
    public instanceDB(db: AngularIndexedDB, version: number) {

        const subject = new Subject<any>();

        db.openDatabase(version, (evt) => {

            let objectStore = evt.currentTarget.result.createObjectStore('dashboard', { keyPath: "id", autoIncrement: true });

            objectStore.createIndex("name", "name", { unique: false });
            objectStore.createIndex("state", "state", { unique: false });
            objectStore.createIndex("message", "message", { unique: false });

        }).then(() => {
            subject.next(db);
        }, (error) => {
            subject.next(null);
        });
        return subject.asObservable();

    }

}
