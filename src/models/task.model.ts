export class Task {
    id?: number;
    name: string;
    state: string;
    message: string;
}