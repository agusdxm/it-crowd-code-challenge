# it-crowd-code-challenge

Mobile application for IT Crowd code challenge. It is build on Ionic.

## Dependencies
  * Angular-cli (+ v1.6.2)
  * NPM (+v5.6.0)
  * Ionic (+3.2.0)
  * angular2-indexeddb (1.2.2)

## Set up environment

1. Clone repository and install dependencies


* $ git clone https://bitbucket.org/agusdxm/it-crowd-code-challenge
* $ cd it-crowd-code-challenge
* $ npm install

2. Run Ionic application.

Local environment 

* $ npm run start

3. Open [http://location:8100](http://location:8100) on your browser

To build android:

* npm run build

This will generate a build version of the Ionic application under the `/platforms/[device]` directory.