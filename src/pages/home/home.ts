import { Component } from '@angular/core';
import { AlertController } from 'ionic-angular';
import { DragulaService } from 'ng2-dragula/dist/';
import { IndexedDBProvider } from '../../providers/IndexedDB-provider';
import { Task } from '../../models/task.model';
import { Subscription } from 'rxjs';
import { AngularIndexedDB } from 'angular2-indexeddb';


@Component({
    selector: 'page-home',
    templateUrl: 'home.html',
})
export class HomePage {

    private TODO: Task[] = [];
    private IN_PROGESS: Task[] = [];
    private DONE: Task[] = [];

    private myColums: string = 'myColums';

    private subs = new Subscription();
    private db: AngularIndexedDB;

    constructor(public alertCtrl: AlertController,
        private dragulaService: DragulaService,
        private dbProvider: IndexedDBProvider) {
    }


    ionViewDidLoad() {

        // create new DB instance called newDb.
        this.db = this.dbProvider.createIndexedDB('newDb', 1)

        this.isLoad();
        this.listenDragulaChanges();
    }

    /**
     * This method it's to subscribe to dragula drop events and change DB state.
     */
    public listenDragulaChanges() {

        this.subs.add(this.dragulaService.dropModel(this.myColums)
            .subscribe(({ target, item }) => {

                // This is a workaround to get item.message.
                this.db.getAll('dashboard').then((tasks: Task[]) => {
                    const filteredTask = tasks.filter((task) => {
                        if (task.id)
                            return task.id === item.id
                    })

                    this.db.update('dashboard', { id: item.id, name: item.name, state: target.id, message: filteredTask[0].message }).then(() => {
                        this.isLoad();
                    }, (error) => {
                        console.log(error);
                    });
                });

            })
        );
    }

    /**
     * Main method, if receive "load Default" parameter will be reset DB data and give some example tasks.
     * 
     * @param loadDefault 
     */
    public isLoad(loadDefault = null) {
        this.dbProvider.instanceDB(this.db, this.db.dbWrapper.dbVersion).subscribe(
            () => {
                if (loadDefault) {
                    this.db.clear('dashboard').then(() => {

                        // Adding some default Tasks.
                        this.db.add('dashboard', { name: 'Task 1', state: 'TODO', message: 'This task have total size' })
                        this.db.add('dashboard', { name: 'Task 2', state: 'DONE', message: 'The detail of task 2!' })
                        this.db.add('dashboard', { name: 'Task 3', state: 'TODO', message: 'Very time consuming task' })
                        this.db.add('dashboard', { name: 'Task 4', state: 'IN_PROGESS', message: 'New achievement places' })
                        this.loadTasks();

                    }, (error) => {
                        console.log(error);
                    });
                }

                this.loadTasks();

            },
            (err: any) => {
                console.log('*** error');
                console.log(err);
            });
    }

    /**
     * Load all the tasks in array columns.
     */
    public loadTasks(asd: any = null) {

        this.db.getAll('dashboard').then((tasks: Task[]) => {

            if (tasks) {

                this.TODO = []
                this.IN_PROGESS = []
                this.DONE = []

                tasks.forEach(task => {
                    switch (task.state) {
                        case 'TODO':
                            this.TODO.push(task)
                            break;
                        case 'IN_PROGESS':
                            this.IN_PROGESS.push(task)
                            break;
                        case 'DONE':
                            this.DONE.push(task)
                            break;
                        default:
                            break;
                    }
                });
            }
            console.log(tasks);
        }, (error) => {
            console.log(error);
        });
    }

    /**
     * Add a Task.
     * @param column 
     */
    public onAddTask() {

        this.db.getAll('dashboard').then((tasks: Task[]) => {

            // TODO
            // The task name is generated from tasks.length, this is not the best approach, but allows generate task name dynamically.
            this.db.add('dashboard', { name: `Task ${tasks.length + 1}`, state: 'TODO', message: '' }).then(() => {
                this.loadTasks();
            }, (error) => {
                console.log(error);
            });
        });
    }

    /**
     *  Edit a task.
     * 
     * @param newValue 
     * @param task 
     */
    public onEditTask(newValue: any, task: Task) {

        this.db.update('dashboard', { name: task.name, state: task.state, message: newValue, id: task.id })
    }

    /**
     * Remove a Task.
     * @param task 
     */
    removeTask(task: Task) {

        this.db.delete('dashboard', task.id).then(() => {
            this.loadTasks();
        }, (error) => {
            console.log(error);
        });
    }

    /**
     * Reset all Tasks to default.
     */
    LoadDefault() {

        this.isLoad(true)
    }

}
